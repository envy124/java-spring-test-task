package com.example.demo.services;

import com.example.demo.dto.CreateOrderDto;
import com.example.demo.entities.OrderEntity;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    Long createOrder(CreateOrderDto order);
    OrderEntity updateOrder(OrderEntity order);
    Long deleteOrder(Long orderId);
    List<OrderEntity> getOrders();
    Optional<OrderEntity> getOrder(Long orderId);
}
