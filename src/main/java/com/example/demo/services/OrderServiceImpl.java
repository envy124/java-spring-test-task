package com.example.demo.services;

import com.example.demo.dto.CreateOrderDto;
import com.example.demo.entities.OrderEntity;
import com.example.demo.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orders;

    @Override
    public Long createOrder(CreateOrderDto order) {
        OrderEntity object = new OrderEntity();
        object.setAddress(order.address);
        object.setClient(order.client);
        object.setDate(order.date);
        OrderEntity newOrder = orders.save(object);
        return newOrder.getId();
    }

    @Override
    public OrderEntity updateOrder(OrderEntity order) {
        return orders.save(order);
    }

    @Override
    public Long deleteOrder(Long orderId) {
        orders.deleteById(orderId);
        return orderId;
    }

    @Override
    public List<OrderEntity> getOrders() {
        return orders.findAll();
    }

    @Override
    public Optional<OrderEntity> getOrder(Long orderId) {
        return orders.findById(orderId);
    }
}
