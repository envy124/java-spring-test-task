package com.example.demo.controllers;

import com.example.demo.dto.CreateOrderDto;
import com.example.demo.entities.OrderEntity;
import com.example.demo.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
class OrderController {

    @Autowired
    private OrderService orders;

    @PostMapping("/order")
    public Long createOrder(@RequestBody() CreateOrderDto createOrderDto) {
        return orders.createOrder(createOrderDto);
    }

    @PutMapping("/order")
    OrderEntity updateOrder(OrderEntity order) {
        return orders.updateOrder(order);
    }

    @DeleteMapping("/order/{orderId}")
    Long deleteOrder(@PathVariable Long orderId) {
        return orders.deleteOrder(orderId);
    }

    @GetMapping("/orders")
    List<OrderEntity> getOrders() {
        return orders.getOrders();
    }

    @GetMapping("/orders/{orderId}")
    public Optional<OrderEntity> getOrderById(@PathVariable Long orderId) {
        return orders.getOrder(orderId);
    }
}