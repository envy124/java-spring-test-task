package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDate;

public class CreateOrderDto {
    public String client;

    @JsonFormat(pattern="yyyy-MM-dd")
    public LocalDate date;

    public String address;
}
