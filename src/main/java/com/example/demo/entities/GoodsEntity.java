package com.example.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class GoodsEntity {
    @Id
    private Long id;

    @Column
    private String name;
    @Column
    private Float price;
}
