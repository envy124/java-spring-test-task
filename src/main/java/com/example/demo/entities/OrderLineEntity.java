package com.example.demo.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class OrderLineEntity {
    @Id
    private Long id;

    @Column
    private Integer orderId;
    @Column
    private Integer goodsId;
    @Column
    private Integer count;
}
